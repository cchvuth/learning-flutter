import 'package:flutter/material.dart';
import '../pages/product/product-detail.dart';
class ProductList extends StatelessWidget {
  final List<String> products;
  ProductList([this.products = const []]);
  @override
  Widget build(BuildContext context) {
    return _buildProductList();
  }

  Widget _buildProductList() {
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: products.length,
    );
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment(0, -1),
            padding: EdgeInsets.all(30),
            child: Text(products[index]),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text('Detail'),
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => ProductDetail()
                  )
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
