import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import './pages/product/product.dart';
void main() {
  // debugPaintSizeEnabled = true;
  // debugPaintBaselinesEnabled = true;
  // debugPaintPointersEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // brightness: Brightness.dark
      ),
      title: 'My App',
      home: Product(),
    );
  }
}
