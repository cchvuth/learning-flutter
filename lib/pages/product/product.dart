import 'package:flutter/material.dart';

import '../../components/product-list.dart';

class Product extends StatefulWidget {
  final List<String> initialProduct;
  Product([this.initialProduct = const []]);

  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  List<String> _products = [];
  void addProduct(product) {
    setState(() {
      _products.add(product);
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Product'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(text: 'Create', icon: Icon(Icons.create)),
              Tab(
                text: 'List',
                icon: Icon(Icons.remove_red_eye),
              )
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            FlatButton(
              child: Text('Add Product'),
              onPressed: () => {this.addProduct('product')},
            ),
            Container(
              child: ProductList(_products),
            ),
          ],
        ),
      ),
    );
  }
}
